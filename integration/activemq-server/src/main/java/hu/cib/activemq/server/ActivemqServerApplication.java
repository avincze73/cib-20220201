package hu.cib.activemq.server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ActivemqServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(ActivemqServerApplication.class, args);
	}

}
