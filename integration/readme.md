# Integration

```bash
curl https://start.spring.io/starter.tgz -d dependencies=web,activemq,lombok,actuator \
-d baseDir=activemq-server -d bootVersion=2.5.5 -d javaVersion=11 \
-d groupId=hu.cib -d artifactId=activemq-server \
-d name=activemq-server \
-d packageName=hu.cib.activemq.server | tar -xzvf -


curl https://start.spring.io/starter.tgz -d dependencies=web,activemq,lombok,actuator \
-d baseDir=kafka-demo -d bootVersion=2.5.5 -d javaVersion=11 \
-d groupId=hu.cib -d artifactId=kafka-demo \
-d name=kafka-demo \
-d packageName=hu.cib.kafka.demo | tar -xzvf -

```


## ActiveMQ
```bash
brew install apache-activemq
/usr/local/opt/activemq/bin/activemq start
```



## Kafka setup
```bash
tar -xzf kafka_2.13-3.1.0.tar
cd kafka_2.13-3.1.0
bin/zookeeper-server-start.sh config/zookeeper.properties
bin/kafka-server-start.sh config/server.properties

# CREATE A TOPIC TO STORE YOUR EVENTS
bin/kafka-topics.sh --create --topic quickstart-events --bootstrap-server localhost:9092

bin/kafka-topics.sh --describe --topic quickstart-events --bootstrap-server localhost:9092

# WRITE SOME EVENTS INTO THE TOPIC
bin/kafka-console-producer.sh --topic quickstart-events --bootstrap-server localhost:9092

# READ THE EVENTS
bin/kafka-console-consumer.sh --topic quickstart-events --from-beginning --bootstrap-server localhost:9092


```