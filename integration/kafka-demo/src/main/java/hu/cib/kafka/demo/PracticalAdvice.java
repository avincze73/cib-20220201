package hu.cib.kafka.demo;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;

@Getter
public class PracticalAdvice {

    private String message;
    private int identifier;


    public PracticalAdvice(@JsonProperty("message") String message,
                           @JsonProperty("identifier") int identifier) {
        this.identifier = identifier;
        this.message = message;
    }
}
