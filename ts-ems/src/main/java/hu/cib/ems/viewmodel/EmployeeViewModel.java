package hu.cib.ems.viewmodel;

import hu.cib.ems.entity.Employee;
import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@Component
public class EmployeeViewModel implements Serializable {

    private List<Employee> employeeList;

    public EmployeeViewModel() {
        this.employeeList = new ArrayList<>();
    }
}
