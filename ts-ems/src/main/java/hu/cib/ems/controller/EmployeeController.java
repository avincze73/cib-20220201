package hu.cib.ems.controller;

import hu.cib.ems.config.TsProperties;
import hu.cib.ems.entity.Employee;
import hu.cib.ems.repository.EmployeeRepository;
import hu.cib.ems.viewmodel.EmployeeViewModel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.support.SessionStatus;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@Controller
@Slf4j
public class EmployeeController {


    @Autowired
    private TsProperties tsProperties;

    @Autowired
    private EmployeeViewModel employeeViewModel;

    @Autowired
    private EmployeeRepository repository;


    @ModelAttribute("titleList")
    public List<String> getTitleList() {
        List<String> titleList = new ArrayList<>();
        titleList.add("Manager");
        titleList.add("CEO");
        titleList.add("Developer");
        return titleList;
    }

    @GetMapping({"/employee"})
    public String getEmployee(Model model) {
        model.addAttribute("entity", new Employee());
        model.addAttribute("pageTitle", "Új alkalmazott felvétele");
        model.addAttribute("nnVersion", tsProperties.getVersion());
        return "employee";
    }

    @GetMapping({"/employeelist"})
    public String getEmployees(Model model, HttpSession session) {
        model.addAttribute("entityList", repository.findAll());
        model.addAttribute("pageTitle", "Alkalmazottak listája");
        model.addAttribute("nnVersion", tsProperties.getVersion());
        return "employees";
    }

    @PostMapping("/employee")
    public String entitySave(@ModelAttribute("entity") @Valid Employee employee,
                             BindingResult errors, SessionStatus status,
                             Model model) {
        if (errors.hasErrors()) {
            return "/employee";
        }
        repository.save(employee);
        status.setComplete();
        return "redirect:employeelist";
    }

}
