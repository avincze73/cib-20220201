package hu.iqjb.web.demo;

import hu.cib.web.demo.controller.GreetingController;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationPropertiesScan;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@WebMvcTest(GreetingController.class)
@ConfigurationPropertiesScan("hu.cib.web.demo.properties")
@Slf4j
public class GreetingControllerTest {

    @Autowired
    private MockMvc mockMvc;


    @Test
    void shouldLoadContext() {
    }

    @Test
    void shouldReturnWithName() throws Exception {
        this.mockMvc.perform(MockMvcRequestBuilders.get("/greeting").param("name", "Attila"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.model().attribute("name", "Attila"))
                .andExpect(MockMvcResultMatchers.view().name("greeting"))
                .andDo(MockMvcResultHandlers.print());
    }


}
