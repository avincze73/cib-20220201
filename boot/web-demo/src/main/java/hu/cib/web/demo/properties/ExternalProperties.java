package hu.cib.web.demo.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ExternalProperties {

    @Bean
    @ConfigurationProperties(prefix = "externalitem")
    public ExternalItem externalItem() {
        return new ExternalItem();
    }
}
