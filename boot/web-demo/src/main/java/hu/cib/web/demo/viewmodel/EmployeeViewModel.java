package hu.cib.web.demo.viewmodel;

import hu.cib.web.demo.controller.Employee;
import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Component;
import org.springframework.web.context.annotation.SessionScope;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@SessionScope
@Component
public class EmployeeViewModel implements Serializable {

    private List<Employee> employeeList;

    public EmployeeViewModel() {
        this.employeeList = new ArrayList<>();
    }
}
