package hu.cib.autoconfiguration.service;

public interface SimpleService {

    public String serve();

}
