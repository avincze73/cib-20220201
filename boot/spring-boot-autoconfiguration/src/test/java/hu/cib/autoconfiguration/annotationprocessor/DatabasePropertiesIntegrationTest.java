package hu.cib.autoconfiguration.annotationprocessor;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;

@SpringBootTest(classes = AnnotationProcessorApplication.class)
@TestPropertySource("classpath:databaseproperties-test.properties")
public class DatabasePropertiesIntegrationTest {

    @Autowired
    private DatabaseProperties databaseProperties;

    @Test
    public void whenSimplePropertyQueriedThenReturnsPropertyValue() throws Exception {
        Assertions.assertEquals( "cibuser", databaseProperties.getUsername());
        Assertions.assertEquals("titkos123", databaseProperties.getPassword());
    }
    
    @Test
    public void whenNestedPropertyQueriedThenReturnsPropertyValue() throws Exception {
        Assertions.assertEquals("127.0.0.1", databaseProperties.getServer().getIp());
        Assertions.assertEquals("3306", Integer.toString(databaseProperties.getServer().getPort()));
    }   

}
