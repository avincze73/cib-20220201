# Samples

## Generating projects
```bash
curl https://start.spring.io/starter.tgz -d dependencies=web,actuator,lombok,validation,jpa \
-d baseDir=simple-boot -d bootVersion=2.5.5 -d javaVersion=11 \
-d groupId=hu.cib -d artifactId=simple-boot \
-d name=simple-boot \
-d packageName=hu.cib.simple.boot | tar -xzvf -

```

