# Setup Ubuntu 20.04

## Post installation tasks
```bash
adduser cib
usermod -aG sudo cib
sudo apt-get update
sudo apt upgrade -y
sudo apt install -y curl git vim maven

sudo apt install -y zsh
zsh --version
sudo usermod -s $(which zsh) cib
sudo chsh -s $(which zsh)
echo $SHELL
sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"

mvn -v
git config --global credential.helper store
```


## Installing docker
```bash
sudo apt-get install -y apt-transport-https ca-certificates curl gnupg-agent software-properties-common
sudo curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
sudo apt-get update
sudo apt install -y docker-ce docker-ce-cli containerd.io docker-compose
sudo usermod -aG docker cib
sudo systemctl start docker
sudo systemctl enable docker

docker -v
docker info
docker images
docker ps -a
```



## Installing microk8s
```bash
sudo snap install microk8s --classic
sudo usermod -a -G microk8s cib
sudo chown -f -R cib ~/.kube
export LC_ALL=C.UTF-8
export LANG=C.UTF-8
sudo microk8s enable dns dashboard storage ingress

microk8s kubectl get all --all-namespaces
token=$(microk8s kubectl -n kube-system get secret | grep default-token | cut -d " " -f1)
microk8s kubectl -n kube-system describe secret $token

```



## Installing kubectl
```bash
sudo apt-get update && sudo apt-get install -y apt-transport-https gnupg2 curl
curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add -
echo "deb https://apt.kubernetes.io/ kubernetes-xenial main" | sudo tee -a /etc/apt/sources.list.d/kubernetes.list
sudo apt-get update
sudo apt install -y kubectl

kubectl version
# Explain the missing Server Version
```


## Installing mongo
```bash
sudo wget -qO - https://www.mongodb.org/static/pgp/server-5.0.asc | sudo apt-key add -
sudo echo "deb [ arch=amd64,arm64 ] https://repo.mongodb.org/apt/ubuntu focal/mongodb-org/5.0 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-5.0.list
sudo apt-get update
sudo apt-get install -y mongodb-org
sudo systemctl start mongod
```





[Főoldal](../README.md)