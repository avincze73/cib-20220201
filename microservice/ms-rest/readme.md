# Creating docker image
```bash
mvn clean package
docker-compose -f docker-compose-basic.yaml build
docker-compose -f docker-compose-layered.yaml build

jar tf target/ms-rest-0.0.1-SNAPSHOT.jar
java -Djarmode=layertools -jar target/ms-rest-0.0.1-SNAPSHOT.jar list
java -Djarmode=layertools -jar target/ms-rest-0.0.1-SNAPSHOT.jar extract
```



```bash
curl -i \ 
-H "Accept: application/json" \ 
-H "Content-Type:application/json" \ 
-X POST --data  '{"username": "johnny", "password": "password"}' "https://localhost:8001/ms-rest/content"
   
curl -H "Content-Type: application/json" -H "Accept: application/json"\
    --request POST \
    --data '{"username": "johnny", "password": "password"}' \
    http://localhost:8001/ms-rest/content | jq 
    
curl -H "Content-Type: application/json" -H "Accept: application/xml"\
    --request POST \
    --data '{"username": "johnny", "password": "password"}' \
    http://localhost:8001/ms-rest/content     
    
curl --request GET \
    http://localhost:8001/ms-rest/api/departments       
    
curl -H "accept-language: eng" \
    --request GET \
    http://localhost:8001/ms-rest/greeting 
      
```


# Versioning

What is the Contract between the API and the Client?
- URIs are not part of the contract! The client should only know a single URI – the entry point to the API. All other URIs should be discovered while consuming the API.
- Media Type definitions are part of the contract.

What to version?
- URI Versioning – version the URI space using version indicators
- Media Type Versioning – version the Representation of the Resource
> when changes need to be introduced in the API, a new URI space needs to be created.

```bash
http://host/v1/users
http://host/v1/privileges


http://host/v2/users
http://host/v2/privileges

curl  --header "Accept: application/vnd.sprint.api.v1+json" \
--request GET \
http://localhost:8001/cib-rest/items  


curl  --header "Accept: application/vnd.sprint.api.v2+json" \
--request GET \
http://localhost:8001/cib-rest/items  


```





# HATEOAS (Hypertext as the Engine of Application State)
```bash

```