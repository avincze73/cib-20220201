package hu.cib.ms.rest;

import hu.cib.ms.dm.entity.Address;
import hu.cib.ms.dm.entity.Department;
import hu.cib.ms.dm.service.DepartmentService;
import org.apache.tomcat.util.codec.binary.Base64;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.annotation.Rollback;

import java.nio.charset.Charset;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
//@Slf4j
//@ActiveProfiles("test")
//@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class DepartmentSystemTest {

    @Autowired
    private TestRestTemplate testRestTemplate;

    @Autowired
    private DepartmentService departmentService;

    @Test
    @Order(1)
    //@Rollback(false)
    public void shouldFindDepartment() {
        Department department = new Department("name2", new Address("1111", "BP", "street1"));
        department = departmentService.save(department);
        assertThat(department).isNotNull();

        ResponseEntity<Department> response =
                testRestTemplate.getForEntity("/api/departments/1", Department.class);
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getBody()).isNotNull();

    }

}
