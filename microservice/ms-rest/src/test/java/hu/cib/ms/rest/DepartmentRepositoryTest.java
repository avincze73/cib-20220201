package hu.cib.ms.rest;

import hu.cib.ms.dm.entity.Address;
import hu.cib.ms.dm.entity.Department;
import hu.cib.ms.dm.repository.DepartmentRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.ActiveProfiles;

import java.util.Optional;
import java.util.logging.Logger;

import static org.assertj.core.api.Assertions.assertThat;

@DataJpaTest
//@ActiveProfiles("mysqltest")
//@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
public class DepartmentRepositoryTest {

    //@DataJpaTest provides some standard setup needed for testing the persistence layer:

    private static final Logger logger = Logger.getLogger(DepartmentRepositoryTest.class.getName());

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private DepartmentRepository departmentRepository;

    @BeforeEach
    public void setUp() {
        entityManager.clear();
        entityManager.flush();
        Department it = new Department("IT", new Address("1111", "Budapest", "Váci u. 11."));
        entityManager.persist(it);
        entityManager.flush();
    }

    @Test
    public void whenFindByName_thenReturnDepartment() {
        Optional<Department> department = departmentRepository.findByName("IT");
        assertThat(department.get().getName()).isEqualTo("IT");
    }

    @Test
    public void should_find_no_department_if_repository_is_empty() { }

    @Test
    public void should_store_a_department() { }

    @Test
    public void should_find_all_departments() { }

    @Test
    public void should_find_department_by_id() { }

    @Test
    public void should_find_department_by_name() { }

    @Test
    public void should_update_department_by_id() { }

    @Test
    public void should_delete_department_by_id() { }

    @Test
    public void should_delete_all_departments() { }

}
