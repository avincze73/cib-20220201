package hu.cib.ms.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import hu.cib.ms.dm.entity.Department;
import hu.cib.ms.dm.factory.DepartmentFactory;
import hu.cib.ms.dm.service.DepartmentService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.hamcrest.CoreMatchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.doNothing;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

//@WebMvcTest(controllers = DepartmentRestController.class)
@SpringBootTest
//@AutoConfigureWebClient
@AutoConfigureMockMvc
public class DepartmentRestControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private DepartmentService departmentService;

    @Autowired
    private ObjectMapper objectMapper;

    @Test
    void shouldFetchAllDepartments() throws Exception {
        List<Department> departmentList = new ArrayList<>();
        departmentList.add(DepartmentFactory.create("name1", "1111", "street1", "city1"));
        departmentList.add(DepartmentFactory.create("name2", "2222", "street2", "city2"));
        departmentList.add(DepartmentFactory.create("name3", "3333", "street3", "city3"));
        given(departmentService.getAll()).willReturn(departmentList);
        this.mockMvc.perform(get("/api/departments"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.size()", is(departmentList.size())));
    }


    @Test
    void shouldFetchOneDepartmentById() throws Exception {
        long id = 1;
        Department department = new Department("dep1");
        department.setId(id);

        given(departmentService.getById(id)).willReturn(Optional.of(department));

        this.mockMvc.perform(get("/api/departments/{id}", id))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name", is(department.getName())));
    }


    @Test
    void shouldReturn404WhenFindDepartmentById() throws Exception {
        long id = 2;
        given(departmentService.getById(id)).willReturn(Optional.empty());

        this.mockMvc.perform(get("/api/departments/{id}", id))
                .andDo(print())
                .andExpect(status().isNotFound());
    }

    @Test
    void shouldCreateNewDepartment() throws Exception {
        Department department = DepartmentFactory.create("name1", "1111", "street1", "city1");
        long id = 1;
        department.setId(id);
        //given(departmentService.save(any(Department.class))).willReturn(department);
        given(departmentService.save(any(Department.class))).willAnswer((invocation) -> invocation.getArgument(0));

        String json = objectMapper.writeValueAsString(department);
        this.mockMvc.perform(post("/api/departments")
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .content(objectMapper.writeValueAsString(department)))
                .andDo(print())
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.name", is(department.getName())))
                .andExpect(jsonPath("$.id", is(department.getId().intValue())));
    }


    @Test
    void shouldReturn400WhenCreateNewDepartmentWithEmptyName() throws Exception {
        Department department = DepartmentFactory.create("", "1111", "street1", "city1");
        department.setId(1L);

        this.mockMvc.perform(post("/api/departments")
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .content(objectMapper.writeValueAsString(department)))
                .andDo(print())
                .andExpect(status().isBadRequest());
    }


    @Test
    void shouldUpdateDepartment() throws Exception {
        long id = 1;
        Department department = DepartmentFactory.create("name1", "1111", "street1", "city1");
        department.setId(id);
        given(departmentService.getById(id)).willReturn(Optional.of(department));
        given(departmentService.update(any(Department.class))).willAnswer((invocation) -> invocation.getArgument(0));

        this.mockMvc.perform(put("/api/departments/{id}", department.getId())
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .content(objectMapper.writeValueAsString(department)))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name", is(department.getName())))
                .andExpect(jsonPath("$.id", is(department.getId().intValue())));

    }



    @Test
    void shouldReturn404WhenUpdatingNonExistingDepartment() throws Exception {
        long id = 4;
        given(departmentService.getById(id)).willReturn(Optional.empty());
        Department department = new Department("cc");
        department.setId(id);

        this.mockMvc.perform(put("/api/departments/{id}", id)
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .content(objectMapper.writeValueAsString(department)))
                .andDo(print())
                .andExpect(status().isNotFound());

    }



    @Test
    void shouldDeleteDepartment() throws Exception {
        long id = 1;
        Department department = DepartmentFactory.create("name1", "1111", "street1", "city1");
        department.setId(id);
        given(departmentService.getById(id)).willReturn(Optional.of(department));
        doNothing().when(departmentService).deleteById(department.getId());

        this.mockMvc.perform(delete("/api/departments/{id}", department.getId()))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(department.getId().intValue())))
                .andExpect(jsonPath("$.name", is(department.getName())));

    }

    @Test
    void shouldReturn404WhenDeletingNonExistingDepartment() throws Exception {
        long id = 5;
        given(departmentService.getById(id)).willReturn(Optional.empty());

        this.mockMvc.perform(delete("/api/departments/{id}", id))
                .andExpect(status().isNotFound());

    }




}
