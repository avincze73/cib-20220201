package hu.cib.ms.rest;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.InetSocketAddress;
import java.util.Map;

@RestController
@Slf4j
public class ContentController {


    @PostMapping(value = "/content",
            consumes = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
    public LoginFormResponse postResponseContent(
            @RequestBody LoginFormRequest loginForm) {
        return new LoginFormResponse("My content as a response");
    }


    /**
     * If a header named accept-language isn't found in the request,
     * the method returns a “400 Bad Request” error.
     * @param language
     * @return
     */
    @GetMapping("/greeting")
    public ResponseEntity<String> greeting(@RequestHeader("accept-language") String language) {
        // code that uses the language variable
        log.info(language);
        return new ResponseEntity<String>("Hello from rest service", HttpStatus.OK);
    }

    @GetMapping("/default")
    public ResponseEntity<String> evaluateDefaultHeaderValue(
            @RequestHeader(value = "optional-header", defaultValue = "3600") int optionalHeader) {
        return new ResponseEntity<String>(
                String.format("Optional Header is %d", optionalHeader), HttpStatus.OK);
    }


    @GetMapping("/int")
    public ResponseEntity<String> doubleNumber(@RequestHeader(value= "my-number", required = false) int myNumber) {
        return new ResponseEntity<String>(String.format("%d * 2 = %d",
                myNumber, (myNumber * 2)), HttpStatus.OK);
    }

    @GetMapping("/listHeaders")
    public ResponseEntity<String> listAllHeaders(
            @RequestHeader Map<String, String> headers) {
        headers.forEach((key, value) -> {
            log.info(String.format("Header '%s' = %s", key, value));
        });

        return new ResponseEntity<String>(
                String.format(headers.toString()), HttpStatus.OK);
    }

    @GetMapping("/getBaseUrl")
    public ResponseEntity<String> getBaseUrl(@RequestHeader HttpHeaders headers) {
        InetSocketAddress host = headers.getHost();
        String url = "http://" + host.getHostName() + ":" + host.getPort();
        return new ResponseEntity<String>(String.format("Base URL = %s", url), HttpStatus.OK);
    }



}
