package hu.cib.ms.rest;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class LoginFormRequest {
    private String username;
    private String password;
}
