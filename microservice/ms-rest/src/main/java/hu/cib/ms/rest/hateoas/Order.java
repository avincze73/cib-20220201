package hu.cib.ms.rest.hateoas;

import lombok.*;
import org.springframework.hateoas.RepresentationModel;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Order extends RepresentationModel<Order> {
    private String orderId;
    private double price;
    private int quantity;


}