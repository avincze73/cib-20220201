package hu.cib.ms.rest;

import lombok.*;

import javax.validation.constraints.NotBlank;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class LoginRequest {

    @NotBlank
    private String username;

    @NotBlank
    private String password;
}
