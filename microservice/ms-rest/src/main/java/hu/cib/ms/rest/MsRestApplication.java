package hu.cib.ms.rest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication
@EntityScan(basePackages = {"hu.cib.ms.dm.entity"})
@EnableJpaRepositories(basePackages = {"hu.cib.ms.dm.repository", "hu.cib.ms.rest"})
@ComponentScan(basePackages = {"hu.cib.ms.dm.*", "hu.cib.ms.rest", "hu.cib.ms.rest.*", "hu.cib.ms.rest.hateoas"})
@EnableTransactionManagement
public class MsRestApplication {

	public static void main(String[] args) {
		SpringApplication.run(MsRestApplication.class, args);
	}

}
