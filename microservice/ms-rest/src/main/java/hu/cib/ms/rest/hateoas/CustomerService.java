package hu.cib.ms.rest.hateoas;

import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@Service
public class CustomerService {

    private HashMap<String, Customer> customerMap;

    public CustomerService() {

        customerMap = new HashMap<>();

        final Customer customerOne = new Customer("10A", "Jane", "ABC Company", null);
        final Customer customerTwo = new Customer("20B", "Bob", "XYZ Company", null);
        final Customer customerThree = new Customer("30C", "Tim", "CKV Company", null);

        customerMap.put("10A", customerOne);
        customerMap.put("20B", customerTwo);
        customerMap.put("30C", customerThree);

    }


    public List<Customer> allCustomers() {
        return new ArrayList<>(customerMap.values());
    }


    public Customer getCustomerDetail(final String customerId) {
        return customerMap.get(customerId);
    }

}
