package hu.cib.ms.rest;

import hu.cib.ms.dm.entity.Department;
import hu.cib.ms.dm.service.DepartmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = {"/api/departments", "/user/departments", "/admin/departments"})
public class DepartmentRestController {

    @Autowired
    private DepartmentService departmentService;


    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public List<Department> getAllDepartments() {
        return departmentService.getAll();
    }

    @GetMapping("/{id}")
    public ResponseEntity<Department> getDepartmentsById(@PathVariable Long id) {
        return departmentService.getById(id)
                .map( department -> ResponseEntity.ok(department))
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Department createDepartment(@RequestBody @Validated Department department) {
        return departmentService.save(department);
    }


    @PutMapping("/{id}")
    public ResponseEntity<Department> updateDepartment(@PathVariable Long id, @RequestBody Department department) {
        return departmentService.getById(id)
                .map(departmentObj -> {
                    departmentObj.setId(id);
                    return ResponseEntity.ok(departmentService.update(departmentObj));
                })
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Department> deleteDepartment(@PathVariable Long id) {
        return departmentService.getById(id)
                .map(department -> {
                    departmentService.deleteById(id);
                    return ResponseEntity.ok(department);
                })
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

}
