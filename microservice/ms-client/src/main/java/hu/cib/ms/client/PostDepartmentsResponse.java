package hu.cib.ms.client;

import hu.cib.ms.dm.entity.Department;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class PostDepartmentsResponse implements Serializable {
    private Department department;

}
