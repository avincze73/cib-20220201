package hu.cib.ms.client;

import hu.cib.ms.dm.entity.Department;
import hu.cib.ms.dm.entity.Employee;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.support.BasicAuthenticationInterceptor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

@Service
@Slf4j
public class WebClientService {


    public void start(){
        WebClient departmentClient =  WebClient.builder()
                .baseUrl("http://localhost:8082")
                .build();

        Mono<Department> department = departmentClient.get().uri("/save").retrieve()
                .bodyToMono(Department.class);
        log.info(department.toString());

        WebClient employeeClient =  WebClient.builder()
                .baseUrl("http://localhost:8081")
                .build();

        Mono<Employee> employee = employeeClient.get().uri("/save").retrieve()
                .bodyToMono(Employee.class);
        log.info(employee.toString());

        log.info(department.block().toString());
        log.info(employee.block().toString());
    }
}
