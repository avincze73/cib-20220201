package hu.cib.ms.client;

import hu.cib.ms.dm.entity.Department;
import hu.cib.ms.dm.entity.Employee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.support.BasicAuthenticationInterceptor;
import org.springframework.http.client.support.BasicAuthorizationInterceptor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;
import org.springframework.web.client.RestTemplate;

@Service
public class RestTemplateService {

    @Autowired
    private RestTemplate restTemplate;

    public void start(){

        ResponseEntity<Employee> entity = this.restTemplate.getForEntity(
                "http://localhost:8081/save", Employee.class);

        ResponseEntity<Department> entity2 = this.restTemplate.getForEntity(
                "http://localhost:8082/save", Department.class);
    }
}
