package hu.cib.ms.dm.entity;

import javax.persistence.*;

@Entity
public class Task extends NNEntity {
	@Basic(optional = false)
	@Column(name = "name")
	private String name;

	@ManyToOne(optional = false)
	@JoinColumn(name = "participantId", referencedColumnName = "id")
	private Employee participant;

	
	public Task() {
		// TODO Auto-generated constructor stub
	}
	
	public Task(String name, Employee participant) {
		super();
		this.name = name;
		this.participant = participant;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Employee getParticipant() {
		return participant;
	}

	public void setParticipant(Employee participant) {
		this.participant = participant;
	}

}
