package hu.cib.ms.dm.exception;

public class DepartmentRegistrationException extends RuntimeException {
    public DepartmentRegistrationException(String message) {
        super(message);
    }
}
