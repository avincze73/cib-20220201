package hu.cib.ms.dm.factory;

import hu.cib.ms.dm.entity.Address;
import hu.cib.ms.dm.entity.Department;

public class DepartmentFactory {

    //Facade
    public static Department create(String name, String zip,
                                    String street, String city){
        return  new Department(name, new Address(zip, city, street));
    }
}
