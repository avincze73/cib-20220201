package hu.cib.ms.dm.service;

import hu.cib.ms.dm.entity.Address;
import hu.cib.ms.dm.entity.Department;
import hu.cib.ms.dm.exception.DepartmentRegistrationException;
import hu.cib.ms.dm.factory.DepartmentFactory;
import hu.cib.ms.dm.repository.DepartmentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class DepartmentService {

    @Autowired
    private DepartmentRepository departmentRepository;


    public Optional<Department> getByName(String name) {
        return departmentRepository.findByName(name);
    }

    @Transactional
    public Department save(Department department) {
        return departmentRepository.save(department);
    }


    @Transactional
    public Department save(String name, Address address) {
        return departmentRepository.save(new Department(name, address));
    }

    @Transactional
    public Department save(String name, String zip, String street, String city) {
        Department department = DepartmentFactory.create(name, zip, street, city);
        Optional<Department> departmentOptional = departmentRepository.findByName(name);
        if (departmentOptional.isPresent()){
            throw new DepartmentRegistrationException("Department already exists");
        }
        return departmentRepository.save(department);
    }



    public Department update(Department department) {
        return departmentRepository.save(department);
    }

    public List<Department> getAll() {
        return departmentRepository.findAll();
    }

    public Optional<Department> getById(Long id) {
        return departmentRepository.findById(id);
    }

    public void deleteById(Long id){
        departmentRepository.deleteById(id);
    }




}
