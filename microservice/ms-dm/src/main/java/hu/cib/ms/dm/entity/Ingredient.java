package hu.cib.ms.dm.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Embeddable;
import javax.persistence.Embedded;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Embeddable
@Getter
@Setter
public class Ingredient {
    @Embedded
    private Quantity quantity;
    @ManyToOne
    @JoinColumn(name="productId")
    private Product product;
}
