package hu.cib.ms.dm.entity;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.util.Date;

@Embeddable
@AllArgsConstructor
@NoArgsConstructor
public class ProjectPeriod {
    @Temporal(TemporalType.DATE)
    @Column(name = "startDate")
    private Date startDate;
    @Temporal(TemporalType.DATE)
    @Column(name = "endDate")
    private Date endDate;
}
