
## Using dockerfile
```bash
#Building the redis image
docker build .
docker run IMAGEID
#Explain the image creation mechanism through redis and gcc
#Explain rebuild with cache
#Replace the order of redis and gcc installation
#Explain how to tag an image and use custom image names
docker build -t avincze73/redis-server:latest . 
docker build -t avincze73/myredis:v1 .
```
