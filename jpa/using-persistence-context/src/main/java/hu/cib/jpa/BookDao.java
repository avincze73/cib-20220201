package hu.cib.jpa;

public interface BookDao {
    public void save(Book book);
}
