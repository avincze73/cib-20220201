package hu.cib.jpa;

public interface DepartmentDao {
    public void save(Department department);
}
