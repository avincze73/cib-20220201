/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.cib.project;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author avincze
 */
@Service
@Transactional
public class RoleService2 {
    @Autowired
    private RoleRepositorySpring roleRepository;
    
    public void save(Role role){
        roleRepository.save(role);
    }
}
