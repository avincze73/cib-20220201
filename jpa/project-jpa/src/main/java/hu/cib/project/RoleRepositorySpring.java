package hu.cib.project;

import org.springframework.data.repository.CrudRepository;


public interface RoleRepositorySpring extends CrudRepository<Role, Long> {

}
