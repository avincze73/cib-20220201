package hu.cib.mybatis;


import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Article {
    private Long id;
    private String title;
    private String author;
}
