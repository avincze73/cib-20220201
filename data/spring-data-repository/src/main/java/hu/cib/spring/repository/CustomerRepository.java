package hu.cib.spring.repository;

import java.util.List;

public interface CustomerRepository {
    Customer save(Customer account);

    List<Customer> findAll();
}
