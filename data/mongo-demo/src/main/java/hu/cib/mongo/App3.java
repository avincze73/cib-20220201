package hu.cib.mongo;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.util.logging.Logger;

/**
 * Hello world!
 */
public class App3 {

    private static final Logger log = Logger.getLogger(App3.class.getName());


    public static void main(String[] args) {
        AnnotationConfigApplicationContext applicationContext = new AnnotationConfigApplicationContext(MongoRepositoryConfig.class);

        PersonService personService = applicationContext.getBean("personService", PersonService.class);
        personService.save();
        personService.getAll();


    }
}
