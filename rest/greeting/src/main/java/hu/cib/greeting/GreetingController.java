package hu.cib.greeting;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Slf4j
public class GreetingController {

    @Autowired
    private InstanceInformationService service;

    @GetMapping(path = "/")
    public String imUpAndRunning() {
        log.info("{healthy:true,app:greeting}");
        return "{healthy:true,app:greeting}";
    }

    @GetMapping(path = "/greetings")
    public String greetingsFromInstance() {
        log.info("Greetings from instance " + service.retrieveInstanceInfo());
        return "Greetings from instance " + service.retrieveInstanceInfo();
    }

}
