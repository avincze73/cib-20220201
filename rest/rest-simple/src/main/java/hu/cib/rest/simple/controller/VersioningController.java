package hu.cib.rest.simple.controller;

import hu.cib.rest.simple.dto.VersionRequest;
import hu.cib.rest.simple.dto.VersionRequestV2;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Slf4j
public class VersioningController {

    @GetMapping(
            value = "/items",
            produces = "application/vnd.sprint.api.v1+json"
            //produces = MediaType.APPLICATION_JSON_VALUE
    )
    public VersionRequest getItemv1() {
        return new VersionRequest("12");
    }

    @GetMapping(
            value = "/items",
            produces = "application/vnd.sprint.api.v2+json"
            //produces = MediaType.APPLICATION_JSON_VALUE
    )
    public VersionRequestV2 getItemv2() {
        return new VersionRequestV2("attila");
    }
}
