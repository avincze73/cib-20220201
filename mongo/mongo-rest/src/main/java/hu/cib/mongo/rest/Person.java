package hu.cib.mongo.rest;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Getter
@Setter
@ToString
@Document(collection = "persons")
public class Person {

	@Id private String id;

	private String firstName;
	private String lastName;

}
