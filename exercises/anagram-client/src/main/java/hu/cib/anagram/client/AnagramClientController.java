package hu.cib.anagram.client;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AnagramClientController {


    @Autowired
    private AnagramServerProxy anagramServerProxy;


    @GetMapping("/api/anagram/{word}")
    public ResponseEntity<AnagramResponse> anagram(@PathVariable String word){
        AnagramResponse result = new AnagramResponse();
        result.setOriginal(word);
        result.setAnagramList(anagramServerProxy.anagram1(word).getBody().getAnagramList());
        return ResponseEntity.ok(result);
    }
}
