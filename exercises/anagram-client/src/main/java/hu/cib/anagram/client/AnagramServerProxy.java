package hu.cib.anagram.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

@FeignClient(name = "anagramserver", url = "${ANAGRAM_SERVER_URL:http://localhost}:9101/anagram-server")
public interface AnagramServerProxy {


    @GetMapping(value = "/api/anagram2/{word}")
    ResponseEntity<AnagramResponse> anagram1(@PathVariable String word) ;

}
