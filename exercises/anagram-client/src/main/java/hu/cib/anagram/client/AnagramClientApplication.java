package hu.cib.anagram.client;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
public class AnagramClientApplication {

	public static void main(String[] args) {
		SpringApplication.run(AnagramClientApplication.class, args);
	}

}
