package hu.cib;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        ApplicationContext applicationContext =
                new ClassPathXmlApplicationContext("beans.xml");
        EmployeeService employeeService =
                applicationContext.getBean(EmployeeService.class);
        Employee employee = new Employee();
        employee.setId(6);
        employeeService.add(employee);
        employeeService.findAll().forEach(System.out::println);
    }
}
