package hu.cib;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class EmployeeService {

    private EmployeeRepository employeeRepository;

    public void add(Employee employee){
        employeeRepository.add(employee);
    }

    public List<Employee> findAll(){
        return employeeRepository.findAll();
    }

}
