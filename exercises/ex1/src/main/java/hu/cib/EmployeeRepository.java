package hu.cib;

import java.util.List;

public interface EmployeeRepository {
    void add(Employee employee);
    List<Employee> findAll();
}
