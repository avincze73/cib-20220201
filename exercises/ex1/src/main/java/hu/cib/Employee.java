package hu.cib;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class Employee {
    private Integer id;
    private String title;
    private String firstName;
    private String lastName;
    private String loginName;
    private String password;
    private Integer salary;

}
