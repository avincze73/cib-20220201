# Exercises


## Anagrams

Based on the dictionary.txt file write a rest service application with the following functionality:
- Give all anagrams of an english word found in dictionary.txt. Create as many rest endpoint as required for flexible usage (GET, POST, Pathvariable, RequestParam).
Use HttpStatus to inform the caller about the outcome of service call.
- Give all anagram groups member with a specified item number. An anagram group contains words that are anagrams of each other. Item number means how many words are in the group.
Create a rest service that gets the item number and sends back the anagram group members. Use as many rest endpoint as required for flexible usage.
- Write tests to the functionalities.
- Deploy this rest service application to kubernetes cluster and expose the endpoints outside the cluster.

```bash
curl https://start.spring.io/starter.tgz -d dependencies=web,actuator \
-d baseDir=anagram-server -d bootVersion=2.5.5 -d javaVersion=11 \
-d groupId=hu.cib -d artifactId=anagram-server \
-d name=anagram-server \
-d packageName=hu.cib.anagram.server | tar -xzvf -



curl https://start.spring.io/starter.tgz -d dependencies=web,actuator,lombok \
-d baseDir=anagram-client -d bootVersion=2.5.5 -d javaVersion=11 \
-d groupId=hu.cib -d artifactId=anagram-client \
-d name=anagram-client \
-d packageName=hu.cib.anagram.client | tar -xzvf -
```