package hu.cib.anagram.server;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Word extends NNEntity {

	@Column(name = "content")
	private String content;

	@Column(name = "sortedContent")
	private String sortedContent;


}
