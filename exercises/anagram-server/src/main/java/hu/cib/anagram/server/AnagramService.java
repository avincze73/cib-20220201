package hu.cib.anagram.server;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class AnagramService {

    @Value("${dictionaryTxt}")
    private Resource resource;


    private List<String > dictionary = new ArrayList<>();

    @Autowired
    private WordRepository wordRepository;


    private String lexicographicalOrder(String input) {
        return input.chars()
                .sorted()
                .mapToObj(i -> (char) i)
                .map(String::valueOf)
                .collect(Collectors.joining());
    }

    @PostConstruct
    protected void init(){
        try (BufferedReader buffer = new BufferedReader(new InputStreamReader(resource.getInputStream()));
             Stream<String> stream = buffer.lines()) {
            stream.forEach( word -> dictionary.add(word));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public AnagramResponse anagramsOfAWord(String word) {
        AnagramResponse anagramResponse = new AnagramResponse();
        anagramResponse.setOriginal(word);
        String ordered = lexicographicalOrder(word);
        List<String> result = dictionary.stream().filter(s -> lexicographicalOrder(s).equals(ordered))
                .collect(Collectors.toList());
        anagramResponse.setAnagramList(result);
        return anagramResponse;
    }

    public AnagramResponse anagramsOfAWord2(String word) {
        AnagramResponse anagramResponse = new AnagramResponse();
        anagramResponse.setOriginal(word);
        String ordered = lexicographicalOrder(word);
        List<String> result = wordRepository.findBySortedContent(ordered).stream().map(Word::getContent).collect(Collectors.toList());
        anagramResponse.setAnagramList(result);
        return anagramResponse;
    }

    public AnagramResponse2 anagramGroups(String count) {
        AnagramResponse2 anagramResponse = new AnagramResponse2();
        return anagramResponse;
    }
}
