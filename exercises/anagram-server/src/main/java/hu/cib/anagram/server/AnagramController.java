package hu.cib.anagram.server;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.InetSocketAddress;
import java.util.Map;

@RestController
@Slf4j
public class AnagramController {

    @Autowired
    private AnagramService anagramService;

    @GetMapping(value = "/api/anagram/{word}")
    public ResponseEntity<AnagramResponse>  anagram1(@PathVariable String word) {
        AnagramResponse response = anagramService.anagramsOfAWord(word);
        return  ResponseEntity.ok(response);
    }

    @GetMapping(value = "/api/anagram2/{word}")
    public ResponseEntity<AnagramResponse>  anagram2(@PathVariable String word) {
        AnagramResponse response = anagramService.anagramsOfAWord2(word);
        return  ResponseEntity.ok(response);
    }

    @GetMapping(value = "/api/anagram3/{count}")
    public ResponseEntity<AnagramResponse2>  anagram3(@PathVariable String count) {
        AnagramResponse2 response = anagramService.anagramGroups(count);
        return  ResponseEntity.ok(response);
    }

}
