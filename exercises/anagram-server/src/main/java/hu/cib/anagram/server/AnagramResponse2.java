package hu.cib.anagram.server;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class AnagramResponse2 {
    private String count;
    private List<String> anagramGroupList;

    public AnagramResponse2() {
        this.anagramGroupList = new ArrayList<>();
    }
}
