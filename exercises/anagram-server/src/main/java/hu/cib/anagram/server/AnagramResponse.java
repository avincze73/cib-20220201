package hu.cib.anagram.server;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class AnagramResponse {
    private String original;
    private List<String> anagramList;

    public AnagramResponse() {
        this.anagramList = new ArrayList<>();
    }
}
