package hu.cib.anagram.server;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class AnagramServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(AnagramServerApplication.class, args);
	}


}
