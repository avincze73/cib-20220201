package hu.cib.anagram.server;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Component
public class DictionaryLoader implements CommandLineRunner {

    @Value("${dictionaryTxt}")
    private Resource resource;

    @Autowired
    private WordRepository wordRepository;

    @Override
    public void run(String... args) throws Exception {
        if (wordRepository.count() != 0) {
            return;
        }

        try (BufferedReader buffer = new BufferedReader(new InputStreamReader(resource.getInputStream()));
             Stream<String> stream = buffer.lines()) {
            stream.forEach( word -> wordRepository.save(new Word(word, lexicographicalOrder(word))));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private String lexicographicalOrder(String input) {
        return input.chars()
                .sorted()
                .mapToObj(i -> (char) i)
                .map(String::valueOf)
                .collect(Collectors.joining());
    }
}
