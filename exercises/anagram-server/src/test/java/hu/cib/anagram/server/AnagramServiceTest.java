package hu.cib.anagram.server;

import org.junit.jupiter.api.Test;
import org.mockito.BDDMockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;

@SpringBootTest
public class AnagramServiceTest {

    @Autowired
    private AnagramService anagramService;

    @Test
    public void shouldSaveDepartment(){

        String word = "secure";
        AnagramResponse anagramResponse = anagramService.anagramsOfAWord(word);
        assertEquals(4, anagramResponse.getAnagramList().size());
    }

}
