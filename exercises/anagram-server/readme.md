## Using configmap
 ```bash
kubectl create configmap cib-config --from-literal=MYSQL_DB=cib
kubectl get configmap cib-config
kubectl describe configmap/cib-config
kubectl edit configmap/cib-config
 ```

## Using secrets
 ```bash
kubectl create secret generic cib-secrets --from-literal=MYSQL_PASSWORD=titkos123
kubectl get secret/cib-secrets
kubectl describe secret/cib-secrets
 ```