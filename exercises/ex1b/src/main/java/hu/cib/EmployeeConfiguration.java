package hu.cib;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = "hu.cib")
public class EmployeeConfiguration {
    
    @Bean
    public Employee employee1(){
        return new Employee(1, "title1", "firstName1", "lastName1", "loginName1", "password1", 1000);
    }

    @Bean
    public Employee employee2(){
        return new Employee(2, "title2", "firstName2", "lastName2", "loginName2", "password2", 2000);
    }

    @Bean
    public Employee employee3(){
        return new Employee(3, "title3", "firstName3", "lastName3", "loginName3", "password3", 3000);
    }

    @Bean
    public Employee employee4(){
        return new Employee(4, "title4", "firstName4", "lastName4", "loginName4", "password4", 4000);
    }

    @Bean
    public Employee employee5(){
        return new Employee(5, "title5", "firstName5", "lastName5", "loginName5", "password5", 5000);
    }

    @Bean
    public InMemoryEmployeeRepository inMemoryEmployeeRepository(){
        InMemoryEmployeeRepository result = new InMemoryEmployeeRepository();
        result.getDatabase().add(employee1());
        result.getDatabase().add(employee2());
        result.getDatabase().add(employee3());
        result.getDatabase().add(employee4());
        result.getDatabase().add(employee5());
        return result;
    }

}
