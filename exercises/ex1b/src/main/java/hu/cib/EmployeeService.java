package hu.cib;

import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Getter
@Setter
@Service
public class EmployeeService {

    @Autowired
    private EmployeeRepository employeeRepository;

    public void add(Employee employee){
        employeeRepository.add(employee);
    }

    public List<Employee> findAll(){
        return employeeRepository.findAll();
    }

}
