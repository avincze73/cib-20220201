package hu.cib;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {

        ApplicationContext applicationContext =
                new AnnotationConfigApplicationContext(EmployeeConfiguration.class);
        EmployeeService employeeService =
                applicationContext.getBean(EmployeeService.class);
        Employee employee = new Employee();
        employee.setId(6);
        employeeService.add(employee);
        employeeService.findAll().forEach(System.out::println);

        System.out.println(applicationContext.getBean("employee1"));

    }
}
