package hu.cib;

import lombok.*;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class Employee {
    private Integer id;
    private String title;
    private String firstName;
    private String lastName;
    private String loginName;
    private String password;
    private Integer salary;

}
