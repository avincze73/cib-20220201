package hu.cib.introduction;

import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Main {

    public static void main(String[] args) {
        ApplicationContext context = new ClassPathXmlApplicationContext("beans.xml");
        GreetingService greetingService = (GreetingService) context.getBean("greetingServiceImpl");
        System.out.println(greetingService.greeting());
        System.out.println(context.hashCode());
        System.out.println(greetingService.hashCode());
        System.out.println("------");

        ApplicationContext context2 = new ClassPathXmlApplicationContext("beans.xml");
        GreetingService greetingService2 = (GreetingService) context2.getBean("greetingServiceImpl");
        System.out.println(greetingService2.greeting());
        System.out.println(context2.hashCode());
        System.out.println(greetingService2.hashCode());
        System.out.println("------");


        ((ConfigurableApplicationContext) context).close();
        ((ConfigurableApplicationContext) context2).close();

    }

}
