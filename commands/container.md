# Containers

## Check Our Docker Install and Config
```bash
docker version
docker info
docker

docker run hello-world

#Overriding default commands of images
docker run busybox echo hello spring
docker run busybox ls
docker run busybox ping www.google.com

#results error because ls is not in the image
docker run hello-world ls 
```

## Working with redis container
```bash
docker run redis
docker exec -it CONTAINERID redis-cli
set myvalue 10
get myvalue 10
docker exec -it CONTAINERID sh

#Starting container with a shell
docker run -it busybox sh 

#Demonstrate container isolation by creating file in a given container which is not seen from another one.
```


## Working with nginx container
```bash
# A new unique id is created
docker container run --publish 80:80 nginx

# Docker is in the background
docker container run --publish 80:80 --detach nginx

# It only shows running containers
docker container ls

docker container stop <container id>

docker container ls

# All containers are listed
docker container ls -a

docker container run --publish 80:80 --detach --name webhost nginx

docker container ls -a

#Checking the log events
docker  logs webhost

docker container top

# This is the process running in the container
# Lists runnning processes in the container
docker container top webhost

docker container --help

docker container ls -a

docker container rm 63f 690 ode

docker container ls

docker container rm -f 63f

docker container ls -a

#Deletes everything
docker system prune 
docker stop CONTAINERID


#immediatelly
docker kill CONTAINERID 
```


## Containers as processes
```bash
docker run --name mongo -d mongo
docker top mongo
ps aux | grep mongod
docker stop mongo
ps aux | grep mongod
docker start mongo
docker top mongo
ps aux | grep mongod
```

## What's Going On In Containers?

```bash
docker container run -d --name nginx nginx

docker container run -d --name mysql -e MYSQL_RANDOM_ROOT_PASSWORD=true mysql

docker container ls

docker container top mysql

docker container top nginx

docker container inspect mysql

docker container stats --help

docker container stats

docker container ls

docker system prune -a
```


## Getting a Shell Inside Containers

```bash
docker container run -help

# If we exit the shell the container stops
docker container run -it --name proxy nginx bash

docker container ls

docker container ls -a

# the default shell is bash
docker container run -it --name ubuntu ubuntu

docker container ls

docker container ls -a

docker container start --help

# attach and interactive mode
docker container start -ai ubuntu

docker container exec --help

docker container exec -it mysql bash

docker container ls

# It is very small sized
docker pull alpine


docker image ls

# It does not have bash
docker container run -it alpine bash

docker container run -it alpine sh
```

## Docker Networks: CLI Management of Virtual Networks

- Each container connected to private virtual network bridge
- Containers on a virtual network can talk to each other


```bash
docker container run -p 80:80 --name webhost -d nginx

docker container port webhost

docker container inspect --format '{{ .NetworkSettings.IPAddress }}' webhost


#CLI management
docker network ls

docker network inspect bridge

docker network ls

docker network create my_app_net

docker network ls

docker network create --help

docker container run -d --name new_nginx --network my_app_net nginx

docker network inspect my_app_net

docker network --help

# we can connect a container to a new network
docker network connect NETWORK_ID CONTAINER_ID

docker container inspect TAB COMPLETION

docker container disconnect TAB COMPLETION

docker container inspect
```


## Docker Networks: DNS and How Containers Find Each Other

```bash
docker container ls

docker network inspect TAB COMPLETION

docker container run -d --name my_nginx --network my_app_net nginx

docker container inspect TAB COMPLETION

#Install with apt-get install iputils-ping
docker container exec -it my_nginx ping new_nginx

docker container exec -it new_nginx ping my_nginx

docker network ls

docker container create --help
```