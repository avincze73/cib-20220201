# cib-20220201

Dr. Vincze Attila



## Topics

- I. 
  - Spring concepts
  - IoC container
  - Dependency injections
  - Spring engine - loaded classes, bootstrap
  - Spring config
  
- II
  - JPA + Spring
  - Transaction
  - JPA relationships - complex, fetch types
  - Bidirectional relationships
  - NoSQL - Mongo

- III.
  - SOAP
  - REST
  - Reactive
  - Microservice based architecture
  - Active-MQ
  - Kafka

- IV.
  - Container based applications
  - Container registries
  - Docker

- V.
  - Kubernetes

- VI.
  - Kubernetes


## Generating projects
```bash
curl https://start.spring.io/starter.tgz -d dependencies=web,actuator,lombok,validation \
-d baseDir=rest-simple -d bootVersion=2.5.5 -d javaVersion=11 \
-d groupId=hu.cib -d artifactId=rest-simple \
-d name=rest-simple \
-d packageName=hu.cib.rest.simple | tar -xzvf -


curl https://start.spring.io/starter.tgz -d dependencies=web,actuator,lombok,validation,data \
-d baseDir=rest-simple -d bootVersion=2.5.5 -d javaVersion=11 \
-d groupId=hu.cib -d artifactId=rest-simple \
-d name=rest-simple \
-d packageName=hu.cib.rest.simple | tar -xzvf -
```


## Microk8s commands
```bash
microk8s.kubectl port-forward -n kube-system service/kubernetes-dashboard 10443:443 --address=0.0.0.0
microk8s dashboard-proxy

scp cib@192.168.0.181:/home/cib/Downloads/admin.config .
kubectl config view

microk8s kubectl port-forward -n kube-system service/kubernetes-dashboard 10443:443 --address 0.0.0.0


curl -kL https://127.0.0.1/
```

Docker material [Docker](https://docker-curriculum.com)

Kubernetes material [Kubernetes](https://auth0.com/blog/kubernetes-tutorial-step-by-step-introduction-to-basic-concepts)

Kubernetes material II. [Kubernetes II](https://ridwanfajar.medium.com/getting-started-with-microk8s-up-and-running-kubernetes-locally-310640dae156)










